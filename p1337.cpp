#include <cstdio>
#include <climits>
using namespace std;

struct Node {
    Node *ch[2]{};
    int z = 0;
    ~Node() {
        if (ch[0]) delete ch[0];
        if (ch[1]) delete ch[1];
    }
};

struct BinTrie {
    Node *root = new Node;
    ~BinTrie() { delete root; }

    void insert(int n) {
        auto p = root;
        for (int i = 31; ; --i) {
            ++p->z;
            if (i < 0) break;
            auto &pn = p->ch[(n >> i) & 1];
            if (!pn) pn = new Node;
            p = pn;
        }
    }

    int select(int k) {
        auto p = root;
        int ans = 0;
        for (int i = 31; i >= 0; --i) {
            int ch0z = p->ch[0] ? p->ch[0]->z : 0;
            if (k > ch0z) {
                k -= ch0z;
                p = p->ch[1];
                ans += ans + 1;
            } else {
                p = p->ch[0];
                ans += ans;
            }
        }
        return ans;
    }
};

int main() {
    BinTrie bt;
    int n;
    scanf("%d", &n);
    while (n--) {
        char c;
        int k;
        scanf(" %c%d", &c, &k);
        switch (c) {
            case 'I': bt.insert(k ^ INT_MIN); break;
            case 'Q': printf("%d\n", bt.select(k) ^ INT_MIN);
        }
    }
    return 0;
}
