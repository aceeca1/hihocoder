#include <cstdio>
#include <climits>
using namespace std;

struct Node {
    int id, val, k, z;
    long long s;
    Node *ch[2];
};

struct Splay {
    Node *null, *root;
    bool isView;

    Splay():
        null(new Node{0, 0, 0, 0, 0, {}}),
        root(new Node{INT_MAX, 0, 0, 1, 0, {null, null}}),
        isView(false) {}

    Splay(Node* null_, Node* root_):
        null(null_), root(root_), isView(true) {}

    void dispose(Node* p) {
        if (p->ch[0] != null) dispose(p->ch[0]);
        if (p->ch[1] != null) dispose(p->ch[1]);
        delete p;
    }

    ~Splay() { if (!isView) { dispose(root); delete null; } }

    Node* push(Node* p) {
        if (p->k) {
            p->val += p->k;
            if (p->ch[0] != null) {
                p->ch[0]->k += p->k;
                collect(p->ch[0]);
            }
            if (p->ch[1] != null) {
                p->ch[1]->k += p->k;
                collect(p->ch[1]);
            }
            p->k = 0;
        }
        return p;
    }

    Node* collect(Node* p) {
        p->z = p->ch[0]->z + p->ch[1]->z + 1;
        p->s = p->ch[0]->s + p->ch[1]->s + p->val + (long long)p->k * p->z;
        return p;
    }

    void zig(bool d) {
        Node *ch = push(root)->ch[d];
        root->ch[d] = null->ch[d];
        null->ch[d] = root;
        root = ch;
    }

    void zigzig(bool d) {
        Node *ch = push(push(root)->ch[d])->ch[d];
        root->ch[d]->ch[d] = null->ch[d];
        null->ch[d] = root->ch[d];
        root->ch[d] = null->ch[d]->ch[!d];
        null->ch[d]->ch[!d] = collect(root);
        root = ch;
    }

    void finish(bool d) {
        Node *&ch = root->ch[!d];
        for (auto i = null->ch[d]; i; ) {
            auto n = i->ch[d];
            i->ch[d] = ch;
            ch = collect(i);
            i = n;
        }
        null->ch[d] = nullptr;
    }

    Node* search(int k) {
        for (;;) {
            bool d = k > root->id;
            if (k == root->id || root->ch[d] == null) break;
            bool dd = k > root->ch[d]->id;
            if (k == root->ch[d]->id || root->ch[d]->ch[dd] == null) {
                zig(d);
                break;
            }
            if (d == dd) zigzig(d); else { zig(d); zig(dd); }
        }
        push(root);
        finish(0);
        finish(1);
        return collect(root);
    }

    void insert(int id, int val) {
        search(id);
        bool d = id > root->id;
        Node *n = new Node{id, val, 0, 0, 0, {null, null}};
        n->ch[d] = root->ch[d];
        root->ch[d] = collect(n);
        collect(root);
    }

    long long query(int a, int b) {
        search(a);
        if (root->ch[1] == null) return 0;
        root->ch[1] = Splay(null, root->ch[1]).search(b);
        long long ans = root->ch[1]->ch[0]->s;
        if (root->ch[1]->id <= b) ans += root->ch[1]->val;
        if (root->id >= a) ans += root->val;
        return ans;
    }

    void modify(int a, int b, int d) {
        search(a);
        if (root->ch[1] == null) return;
        root->ch[1] = Splay(null, root->ch[1]).search(b);
        if (root->ch[1]->ch[0] != null) {
            root->ch[1]->ch[0]->k += d;
            collect(root->ch[1]->ch[0]);
            collect(root->ch[1]);
            collect(root);
        }
        if (root->ch[1]->id <= b) {
            root->ch[1]->val += d;
            collect(root->ch[1]);
            collect(root);
        }
        if (root->id >= a) {
            root->val += d;
            collect(root);
        }
    }

    void remove(int a, int b) {
        search(a);
        if (root->ch[1] == null) return;
        root->ch[1] = Splay(null, root->ch[1]).search(b);
        if (root->ch[1]->ch[0] != null) {
            dispose(root->ch[1]->ch[0]);
            root->ch[1]->ch[0] = null;
            collect(root->ch[1]);
            collect(root);
        }
        if (root->ch[1]->id <= b) {
            auto p = root->ch[1];
            root->ch[1] = p->ch[1];
            delete p;
            collect(root);
        }
        if (root->id >= a) {
            auto p = Splay(null, root->ch[1]).search(INT_MIN);
            p->ch[0] = root->ch[0];
            delete root;
            root = collect(p);
        }
    }
};

int main() {
    int n;
    scanf("%d", &n);
    Splay sp;
    while (n--) {
        char c;
        scanf(" %c", &c);
        switch (c) {
            case 'I': {
                int id, val;
                scanf("%d%d", &id, &val);
                sp.insert(id, val);
            } break;
            case 'Q': {
                int a, b;
                scanf("%d%d", &a, &b);
                printf("%lld\n", sp.query(a, b));
            } break;
            case 'M': {
                int a, b, d;
                scanf("%d%d%d", &a, &b, &d);
                sp.modify(a, b, d);
            } break;
            case 'D': {
                int a, b;
                scanf("%d%d", &a, &b);
                sp.remove(a, b);
            }
        }
    }
    return 0;
}
